# Aufgabe 3: Properties, Indexer und Foreach
## a) (25 Punkte)
Implementieren Sie in C# eine Klasse "Vorlesungsteilnehmer", die zur Verwaltung der FPK-Vorlesung ab WS2015 eingesetzt
werden soll.

Auf dem Klausurbogen soll lediglich der Quellcode der Datei "Vorlesungsteilnehmer.cs" zu finden sein! Es sind keine weiteren
Angaben zur Konfiguration innerhalb von VS.net nötig.

Implementieren Sie die folgenden Member:

| Member | Beschreibung |
| ---- | ---|
| Capacity | Read-Only Property zum Auslesen der maximalen Anzahl von Studenten, die an dieser Veranstaltung teilnehmen können. |
|Capacity| das zugehörige private konstante Feld, das durch den Konstruktor initialisiert werden soll.|
|Count | Read-Only Property zur Ausgabe der zzt. in der Vorlesung verwalteten StudentInnen |
|CountMale | Diesmal nur die männlichen. |
| CountFemale | Diesmal nur die weiblichen. |
| Add(Student) | fügt einen Student zur Vorlesung hinzu. |
| Remove(Student) | entfernt einen Studenten aus der Vorlesung. |

Zusätzlich sollen ein Konstruktor und ein Indexer implementiert werden. Weitere Member – sofern benötigt – sind eigenständig dieser Klasse hinzuzufügen. Es ist freigestellt, wie die Klasse "Vorlesungsteilnehmer" die Studenten intern verwaltet.

Eine Klasse "Student" ist bereits vorhanden. Objekte dieser Klasse sollen in der Klasse "Vorlesungsteilnehmer" verwendet
werden. Die Klasse Student verfügt über ein Property "Gender", über das das Geschlecht ausgelesen werden kann. Ein Konstruktor der Klasse Student erwartet den Namen (string) und das Geschlecht (char) als Parameter.

## b) (5 Punkte)
Implementieren Sie ein Hauptprogramm, welches ihren Code testet. Gehen Sie dabei von einer C#-Konsolenanwendung aus.
