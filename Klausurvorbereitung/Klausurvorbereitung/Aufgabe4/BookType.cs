﻿namespace Klausurvorbereitung.Aufgabe4
{
    public enum BookType
    {
        Roman, Gedicht, Märchen, Sage, Sachbuch
    }
}