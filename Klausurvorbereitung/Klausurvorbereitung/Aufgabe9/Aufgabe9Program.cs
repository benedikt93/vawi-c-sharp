﻿using System;

namespace Klausurvorbereitung.Aufgabe9
{
    public class Aufgabe9Program
    {
        public static void Run()
        {
            var string1 = "Meier, Hans;Heinrichstr. 34 45332 Essen;23.02.2001";
            var string2 = "Müller-Lüdenscheidt, Otto;Berliner Str. 89 45123 Essen;12.04.1987";
            var string3 = "Schmidt, Hans O.;Sommer Allee 1 17643 Lubmin;01.03.1917";
            
            Console.WriteLine("String1: ");
            Console.WriteLine(string1);
            Console.WriteLine("Street: " + AdressParser.SearchStreet(string1));
            Console.WriteLine("Nachname: " + AdressParser.SearchLastName(string1));
            Console.WriteLine("Geburtsdatum: " + AdressParser.SearchBirthDate(string1));
            Console.WriteLine("---------------------------------");
            
            Console.WriteLine("String2: ");
            Console.WriteLine(string3);
            Console.WriteLine("Street: " + AdressParser.SearchStreet(string2));
            Console.WriteLine("Nachname: " + AdressParser.SearchLastName(string2));
            Console.WriteLine("Geburtsdatum: " + AdressParser.SearchBirthDate(string2));
            Console.WriteLine("---------------------------------");
            
            Console.WriteLine("String3: ");
            Console.WriteLine(string3);
            Console.WriteLine("Street: " + AdressParser.SearchStreet(string3));
            Console.WriteLine("Nachname: " + AdressParser.SearchLastName(string3));
            Console.WriteLine("Geburtsdatum: " + AdressParser.SearchBirthDate(string3));
            Console.WriteLine("---------------------------------");
            
        }
    }
}