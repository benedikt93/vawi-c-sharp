using Microsoft.VisualStudio.TestTools.UnitTesting;
using VideoStore;

namespace UnitTestProject
{
    [TestClass]
    public class StatementTest
    {
        [TestMethod]
        public void CreateStatementMultipleMovies()
        {
            Movie m1 = new Movie("Die fabelhafte Welt der Amelie", Movie.Type.REGULAR);
            Movie m2 = new Movie("Titanic 2", Movie.Type.NEW_RELEASE);
            Movie m3 = new Movie("Ferkels großes Abenteuer", Movie.Type.CHILDRENS);

            Customer c1 = new Customer("Vorname Nachname");

            Rental r1 = new Rental(m1, 12);
            Rental r2 = new Rental(m2, 3);
            Rental r3 = new Rental(m3, 5);

            c1.AddRental(r1);
            c1.AddRental(r2);
            c1.AddRental(r3);

            string result = "Rental Record for Vorname Nachname\n"
            + "\tDie fabelhafte Welt der Amelie	17\n"
            + "\tTitanic 2	9\n"
            + "\tFerkels großes Abenteuer	4,5\n"
            + "Amount owed is 30,5\n"
            + "You earned 4 frequent renter points";

            Assert.AreEqual(result, c1.Statement());

            Assert.AreEqual(12, r1.DaysRented);
        }
        
        [TestMethod]
        public void CreateStatementSingleMovie()
        {
            Movie m1 = new Movie("Imitation Game", Movie.Type.NEW_RELEASE);

            Customer c1 = new Customer("Max Muster");
            
            Rental r1 = new Rental(m1, 9);
            c1.AddRental(r1);

            string result = "Rental Record for Max Muster\n"
                            + "\tImitation Game	27\n"
                            + "Amount owed is 27\n"
                            + "You earned 2 frequent renter points";

            Assert.AreEqual(result, c1.Statement());
        }
        
        [TestMethod]
        public void FrequentPointsNewReleaseZeroDays()
        {
            Movie m1 = new Movie("Imitation Game", Movie.Type.NEW_RELEASE);

            Rental r1 = new Rental(m1, 0);
            
            Assert.AreEqual(1, r1.GetFrequentRenterPoints());
        }
        
        [TestMethod]
        public void CreateStatementNoMovie()
        {
            Customer c1 = new Customer("Max Muster");
            string result = "Rental Record for Max Muster\n"
                            + "Amount owed is 0\n"
                            + "You earned 0 frequent renter points";

            Assert.AreEqual(result, c1.Statement());
        }
        
        [TestMethod]
        public void ChangeMovieType()
        {
            Movie m1 = new Movie("Die fabelhafte Welt der Amelie", Movie.Type.REGULAR);
            m1.PriceCode = Movie.Type.NEW_RELEASE;

            Assert.AreEqual(m1.PriceCode, Movie.Type.NEW_RELEASE);
        }
    }
}
