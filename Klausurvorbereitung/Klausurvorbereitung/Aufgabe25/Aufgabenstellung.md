# Aufgabe 25: Metadaten
Sie sind an einem Projekt beteiligt, in dem ein Framework für O/R-Mapping implementiert werden soll. Ihr Vorschlag, Metadaten zur Steuerung des Frameworks einzusetzen wurde von der Projektleitung angenommen. Die Methoden zur Verarbeitung
der Metadaten sind bereits von einem Kollegen auf Basis einer theoretischen Spezifikation implementiert worden.

Die Implementierung könnte u.a. folgendes SQL-Script erzeugen:
````sql
CREATE TABLE [dbo].[contact] (
    [id] [int] (1, 1) NOT NULL,
    [email] [VARCHAR] (80) NOT NULL,
    [address] [VARCHAR] (80) NOT NULL,
    [city] [VARCHAR] (50) NOT NULL,
    [name] [VARCHAR] (50) NOT NULL,
    [age] [int] NOT NULL,
) ON [PRIMARY]
GO
````
## a) (18 Punkte)
hre Aufgabe ist es, drei für das O/R-Mapping der Klasse Contact benötigte Attribute in einer Assembly zu implementieren. Die
Attribute sollen die benötigten Metadaten abbilden, um ein Mapping zwischen Objekten und relationalen Datentabellen zu
erreichen.

Die Implementierung der Attribute soll den Anforderungen an Attributimplementierung genügen (Serialisierbarkeit ist nicht
erforderlich) und abhängig vom Attribut die benötigten Member bereitstellen. Versäumen Sie nicht den gezielten Einsatz des
AttributeUsage-Attributes.

## b) (12 Punkte)
Implementieren zusätzlich Sie eine Klasse Contact, die mit den Attributen versehen ist und die das Pendant zum SQL-Skript
darstellen kann. Neben den Feldern, Properties und Attributen sind keine weiteren Funktionen erforderlich - Es genügt die
Klasse. Assembly-Informationen (using, namespace, etc.) sind an dieser Stelle nicht erforderlich.