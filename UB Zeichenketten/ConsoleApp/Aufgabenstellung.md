# UB 06 Zeichenketten
## Qualifikationsziele
Die Studierenden
- Können ein Programm erstellen, das sich aus mehreren Assemblies
zusammensetzt
- Können die Basisklassen zur Ein- und Ausgabe von Daten über
Textdateien nutzen
- Können die Basisklassenfunktionalität anhand der MSDN
erarbeiten
- Können reguläre Ausdrücke zur Transformation von Textdateien
einsetzen

## Anforderungen
- Zur Architektur: Das Programm soll aus zwei Assemblies bestehen, und zwar aus
einer Konsolenanwendung (exe) zur Ein- und Ausgabe der Daten sowie einer
Klassenbibliothek (dll), die die eigentliche Programmlogik kapselt.
- Die Anwendung soll eine Textdatei (input.txt) öffnen und den Inhalt einlesen.
- Der Inhalt des Textes wird als Parameter an eine Methode in der
Klassenbibliothek (dll) weitergereicht werden.
- Innerhalb der Klassenbibliothek wird die eigentliche Umstrukturierung des
Textes erfolgen.
- Nach erfolgreicher Umwandlung wird das Ergebnis der Transformation an die
Konsolenanwendung zurückgegeben und in eine andere Textdatei (output.txt)
ausgegeben.
- Ein- und Ausgabe sollen auf der Konsole verfolgt werden können.

## Projekt Datei
Enthält
- grundlegenden Aufbau mit
entsprechenden Methoden
und ToDo-Anweisungen
- Input.txt mit den auf der
nachfolgenden Seite
aufgeführten Beispielen

## Dateiformate
**Ausgangsformat: (Beispiele)**
- Hans Meier|Musterstr. 10|45444|Essen|D
- Egon Müller|Berliner Str. 12|1234|Warschau|PL
- Egon Müller-Lüdenscheidt|Breslauer Weg 1252|13544|Berlin|D

**Zielformat:**
- Nachname, Vorname, Straße, Hausnummer, Land, PLZ, Stadt
