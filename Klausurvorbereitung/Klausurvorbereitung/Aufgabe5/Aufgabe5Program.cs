﻿using System;

namespace Klausurvorbereitung.Aufgabe5
{
    public class Aufgabe5Program
    {
        public static void Run()
        {
            var p1 = new EnhancedPoint();
            p1.X = 6;
            p1.Y = 10;
            
            var p2 = new EnhancedPoint(4, 5);
            var p3 = new EnhancedPoint(p2);
            p3.X = 3;
            
            Console.WriteLine("Ausgangssituation");
            Console.WriteLine("p1: " + p1);
            Console.WriteLine("p2: " + p2);
            Console.WriteLine("p3: " + p3);

            Console.WriteLine("Rechnungen");
            Console.WriteLine("p1 + p2 = " +  (p1 + p2));
            Console.WriteLine("p1 - p3 = " +  (p1 - p3));
            Console.WriteLine("p1 == p3 = " +  (p1 == p3));
            Console.WriteLine("p1 == (p3 + p3) = " +  (p1 == p3 + p3));
            
            Console.WriteLine("Rechnungen");
            Console.WriteLine("Distanz zwiscchen p1 und p2: " + p1.DistanceTo(p2));
            Console.WriteLine("Distanz zwiscchen p3 und 0: " + (float)p3);
            
        }
    }
}