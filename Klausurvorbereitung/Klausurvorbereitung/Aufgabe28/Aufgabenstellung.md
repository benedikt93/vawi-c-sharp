# Aufgabe 28: Generics
## a) (15 Punkte)
Implementieren Sie eine Assembly, die einen Stack abbildet.

Innerhalb des Namespaces StackManager soll die generische Klasse *Stack<T>* implementiert werden. Diese Klasse dient
dazu Dokumente zu einem Stack hinzuzufügen und wieder zu entnehmen sowie zur Verwaltung des Stackinhalts. Das Konzept
ähnelt dabei dem in .NET im Kontext des Memory Managements eingesetzten Stack.

Wichtig: Lesen Sie den Text der gesamten Aufgaben, damit sie etwaige Anpassungen frühzeitig bedenken können.

Die Klasse *Stack<T>* hat die folgenden Anforderungen:
- Privates Array-Feld items vom Typ *T*, das die Elemente des Stacks aufnimmt
- Privates Feld stackPointer vom Typ *int*, dass auf die erste freie Position im Array-Feld items zeigt
- Eine Methode *push(T t)* die dazu dient neue Elemente vom Typ *T* zum Stack hinzuzufügen
- Eine Methode *pop()* die das letzte hinzugefügte Element vom Typ *T* zurück gibt und aus dem Stack entfernt
- Eine Methode *reset()* die den Inhalt des Stacks löscht bzw. zurücksetzt
- Eine Überschreibung der *toString()*-Methode, die die *toString()*-Methode aller Elemente auf dem Stack aufruft
und die Ausgaben gesammelt zurück gibt
- Einen Konstruktor, der die Größe des Stacks als *int*-Typ entgegennimmt und den Stack entsprechend initialisiert

## b) (5 Punkte)
Die Klasse Stack soll so erweitert werden, dass nur geeignete Elemente hinzugefügt werden können. Als geeignet gelten alle
Objekte bzw. Klassen, die das Interface *IStack* implementieren. Passen Sie die Klasse *Stack* so an, dass die Anforderung
erfüllt wird.
Das Interface muss nicht implementiert werden und kann als gegeben angenommen werden

## c) (10 Punkte)
Nennen und Erläutern Sie die Vorteile von Generics. Gehen Sie dabei unter anderem auf die Vorteile ein, die sich aus der Verwendung des Typs T anstelle der Verwendung des Typs *Object* in der Klasse *Stack* ergeben.