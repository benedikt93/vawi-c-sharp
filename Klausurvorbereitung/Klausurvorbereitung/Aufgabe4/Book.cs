﻿using System;
using System.Collections;
using System.IO;
using System.Xml.Serialization;

namespace Klausurvorbereitung.Aufgabe4
// namespace ElectronicLibrary
{
    public class Book
    {
        [XmlElement("author")]
        public string Autor { get; set; }

        [XmlElement("title")]
        public string Titel { get; set; }

        [XmlElement("year")]
        public int Erscheinungsjahr { get; set; }

        [XmlElement("type")]
        public BookType Buchtyp { get; set; }

        [XmlArray("chapters"), XmlArrayItem("chapter")]
        public ArrayList Kapitel { get; set; }

        public Book()
        {
        }

        public Book(BookType buchtyp, ArrayList kapitel, string autor, string titel, int erscheinungsjahr)
        {
            Buchtyp = buchtyp;
            Kapitel = kapitel;
            Autor = autor;
            Titel = titel;
            Erscheinungsjahr = erscheinungsjahr;
        }

        public Book(Book book)
            : this(book.Buchtyp, book.Kapitel, book.Autor, book.Titel, book.Erscheinungsjahr)
        {
        }

        public Book(BookType buchtyp, string autor, string titel, int erscheinungsjahr)
            : this(buchtyp, new ArrayList(), autor, titel, erscheinungsjahr)
        {
        }

        public string this[int i]
        {
            get => (string) Kapitel[i];
            set { Kapitel[i] = value; }
        }

        public void Add(string kapitel)
        {
            Kapitel.Add(kapitel);
        }

        public override string ToString()
        {
            var xmlSerializer = new XmlSerializer(typeof(Book));
            using (var stringWriter = new StringWriter())
            {
                xmlSerializer.Serialize(stringWriter, this);
                return stringWriter.ToString();
            }
        }
    }
}