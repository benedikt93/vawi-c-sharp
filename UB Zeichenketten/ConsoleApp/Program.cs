﻿using ParserLibrary;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            const string inputFile = "input.txt";
            const string outputFile = "output.txt";

            var inputList = new List<string>(File.ReadAllLines(inputFile));

            Console.WriteLine("Input:");
            inputList.ForEach(Console.WriteLine);

            var convertedList = AddressParser.convertFileContent(inputList);

            Console.WriteLine("Output:");
            convertedList.ForEach(Console.WriteLine);

            Console.WriteLine(File.ReadAllLines(outputFile).SequenceEqual(convertedList)
                ? "Result is correct"
                : "Result is incorrect");

            Console.Read();
        }
    }
}