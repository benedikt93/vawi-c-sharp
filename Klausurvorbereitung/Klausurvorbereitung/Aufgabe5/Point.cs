﻿namespace Klausurvorbereitung.Aufgabe5
{
    public class Point
    {
        public int X { get; set; }
        public int Y { get; set; }
    }
}