﻿using System.Collections.Generic;
using System.Linq;

namespace Klausurvorbereitung.Aufgabe3
{
    public class Vorlesungsteilnehmer
    {
        private readonly int capacity;
        public int Capacity => capacity;

        
        public int Count => students.Count;

        public int CountMale
        {
            get => students.Where(entry => entry.Geschlecht == '0').Count();
        }

        public int CountFemale => students.Where(entry => entry.Geschlecht == '1').Count();

        private LinkedList<Student> students;

        public Vorlesungsteilnehmer(int capacity)
        {
            this.capacity = capacity;
            students = new LinkedList<Student>();
        }

        public Student this[int i] => students.ToList()[i];

        public void Add(Student student)
        {
            if (students.Count >= Capacity)
                return;
            
            students.AddLast(student);
        }
        public void Remove(Student student) => students.Remove(student);
    }
}