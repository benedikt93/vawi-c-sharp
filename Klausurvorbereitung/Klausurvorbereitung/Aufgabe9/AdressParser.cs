﻿using System.Text.RegularExpressions;

namespace Klausurvorbereitung.Aufgabe9
{
    public class AdressParser
    {
        public static string SearchStreet(string input)
        {
            // var regex = @"([a-zA-Z]+\W*\s)+\d+";
            var regex = @"[a-zA-Z .]+\s\d+";
            return Regex.Match(input, regex).ToString();
        }

        public static string SearchLastName(string input)
        {
            var regex = @"^[\w-]+";
            return Regex.Match(input, regex).ToString();
        }

        public static string SearchBirthDate(string input)
        {
            var regex = @"\d{2}\.\d{2}\.\d{4}";
            return Regex.Match(input, regex).ToString();
        }
    }
}