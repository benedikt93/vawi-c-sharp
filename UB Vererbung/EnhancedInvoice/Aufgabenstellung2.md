# UB 02 Vererbung
## Qualifikationsziele
Die Studierenden
 - Können die grundlegenden Mechanismen der Implementierung von
Vererbungsmechanismen in C# anwenden
 - Können anhand einer textuellen Beschreibung ein Programm
entwickeln, erweitern oder anpassen


## Aufgabe
### Hinweise
 - Im Rahmen eines einfachen Beispiels werden Sie eine bestehende
Klasse um einen Indexer, um Properties und um eine
ToString()-Überladung erweitern, um den Einsatz der Objekte
zu vereinfachen.

 - In Hinblick auf Interoperabilität ist hervorzuheben, dass der
Quellcode der Basis-Klasse nicht zur Verfügung steht. Die Assembly
könnte prinzipiell auch in einer anderen .NET Sprache
implementiert worden sein.

## Aufgabenbeschreibung
 - Gegeben ist eine .NET Assembly, die Klassen enthält, um
Rechnungen objektorientiert abzubilden.
 - Der Quellcode dieser Assembly steht nicht zur Verfügung.
 - Im Namespace der Assembly (Financials) befinden sich die
Klassen Invoice und InvoiceItem. Die Klassen sind
rudimentär („Invoice.xml“) dokumentiert worden und beinhalten
im wesentlichen nur Datenstrukturen.
 - Um die Klassen einfacher nutzen zu können sollen mit Hilfe von
Vererbung verschiedene Aufgaben bearbeitet werden, die einen
komfortableren Einsatz der Rechnungsobjekte ermöglichen.
 - Exemplarische Main-Methoden zum Testen der Invoice- und
EnhancedInvoice-Klassen sind bereits im Projekt enthalten.

## Teilaufgaben
1. EnhancedInvoice und EnhancedInvoiceItem sollen von den vorhandenen
Klassen abgeleitet werden und jeweils um geeignete (Nicht-Standard)
Konstruktoren ergänzt werden, damit eine einfache Objekterzeugung möglich ist.
Die Datei EnhancedInvoice.cs ist bereits in der Solution enthalten.
2. EnhancedInvoiceItem soll um eine ToString() -Überladung ergänzt
werden, um eine Rechnungsposition übersichtlich in einer Textzeile auszugeben
3. EnhancedInvoice soll um die folgenden Elemente ergänzt werden:

   a) Readonly Property „TotalSum“, welches die Rechnungssumme aus den
einzelnen Positionen ermittelt.

   b) Readonly Indexer, der es über ein Integer-Index ermöglicht, die einzelnen
Positionen einer Rechnung auszulesen.

   c) ToString()-Überladung, zur Ausgabe der Gesamtrechnung

   d) Eine Überschreibung (Override) des Properties Items, damit ein
EnhanceInvoiceItem-Array anstatt einer ArrayList zurückgegeben wird.
