# Aufgabe 12: Reguläre Ausdrücke
## a) (9 Punkte)
Implementieren Sie eine Assembly, die der Verwaltung von Kundendaten dient.

Innerhalb des Namespaces *CustomerDatabase* soll die Klasse *Customer* implementiert werden. Beachten Sie bei der Implementierung die zweite Teilaufgabe, damit Sie die Anforderungen frühzeitig bedenken oder vor den jeweiligen Elementen
entsprechend Freiraum lassen.

Für die Klasse gelten die folgenden Anforderungen:
 - Die Klasse Customer enthält die folgenden **Properties**
   - Forename vom Typ string
   - Surname vom Typ string
   - TelephoneNumber vom Typ string
   - EMail vom Typ string
   - *Collection OrderList* vom Typ *Order* (Die Klasse *Order* muss nicht implementiert werden)
   - ReadOnly-Property *SumOfAllOrders* vom Typ double, das die Summe der Preise aller Bestellungen
zurückgibt (die Preise der einzelnen Bestellungen werden im *double*-Property *OrderPrice* in der Klasse
Order gespeichert).
 - Die Klasse Customer soll zudem eine Ausgabe des Namens und der Telefonnummer eines Kunden mit Hilfe der
Überschreibung der *toString()*-Methode unterstützen.

## b) (16 Punkte)
Zur Plausibilitätsprüfung der Kundendaten sollen zwei Methoden in einer neuen Klasse IntegrityCheck im Namespace
CustomerDatabase implementiert werden, die mit Hilfe von regulären Ausdrücken prüfen, ob Telefonnummer und E-MailAdresse im richtigen Format vorliegen. Die Prüfung soll anhand der hier aufgeführten Vorgaben erfolgen. Weitere Prüfungen
oder Bedingungen sind nicht zu Berücksichtigen.
- Die Methode public Boolean checkTelephoneNumber(String number) prüft die folgenden Anforderungen
   - Vorwahl mit mindestens 2 und maximal 5 Zahlen
   - Trennzeichen "-" zwischen Vorwahl und Teilnehmernummer
   - Teilnehmerrufnummer mit mindestens 3 und maximal 7 Zahlen
- Die Methode public Boolean checkEmail(String email) prüft die folgenden Anforderungen
   - Lokaler Teil mit mind. einem Wortzeichen; beliebige Zahlen und Buchstaben möglich;
   max ein "." als Sonderzeichen erlaubt
   - @-Zeichen zur Trennung
   - mind ein Wortzeichen; beliebige Zahlen und Buchstaben; erlaubte Sonderzeichen "-" und "."
   - "." vor Domainendung; mindestens 2 max 5 Kleinbuchstaben

Als Hilfestellung (nur Denkanstoß; es können alle vorhandenen Zeichen genutzt werden) sind die folgenden Sprachbestandteile regulärer Ausdrücke gegeben:

| | Zeichen|
| ---|---|
| . | Beliebiges Zeichen außer \n |
| \n | New Line| 
| \d | Dezimal-Zeichen| 
| \s | White-Space-Zeichen| 
| \w | Wortzeichen| 
| | Quantifizierer| 
| * | 0-n| 
| + | 1-n| 
| ? | 0-1| 
| {n} | N| 