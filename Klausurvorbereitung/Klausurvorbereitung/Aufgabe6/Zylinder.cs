﻿using System;

namespace Klausurvorbereitung.Aufgabe6
{
    public class Zylinder: Kreis
    {
        public Zylinder(int x, int y) : base(x)
        {
            Y = y;
        }

        public override double BerechneFlaeche()
        {
            return 2 * base.BerechneFlaeche() + 2 * X * Math.PI * Y;
        }
    }
}