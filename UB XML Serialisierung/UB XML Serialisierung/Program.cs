﻿using System;
using UBXMLSerialisierung.Bestellung;
using UBXMLSerialisierung.Library;
using UBXMLSerialisierung.Movies;

namespace UBXMLSerialisierung
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            
            // Aufgabe 1
            Console.WriteLine("Aufgabe 1");
            string filename = "outputBooks.xml";
            var book1 = new Book(
                "C# 2012 and .NET 4.5",
                "Christian Nagel et al.",
                BookGenre.Sachbuch,
                2013,
                "978-1-118-31442-5",
                50
                );
            
            var book2 = new Book(
                "Head First Design Patterns",
                " Elisabeth Freeman",
                BookGenre.Sachbuch,
                2004,
                "978-1600330544",
                30
                );
            
            var inventory1 = new Inventory(new[]{book1, book2});
            Console.WriteLine("Inventory 1:");
            Console.WriteLine(inventory1.ToString());
            
            var serializer = new Serializer();
            serializer.ExportToXml(filename, inventory1);

            var inventory2 = serializer.ImportFromXml(filename);
            Console.WriteLine(inventory2.ToString());
            
            // Aufgabe 2
            Console.WriteLine("Aufgabe 2");
            filename = "outputPurchaseOrder.xml";
            var bestellung = new Bestellung.Bestellung(
                    new Adresse(
                        "Peter M.",
                        "Schuler",
                        "Universitätsstr. 9",
                        "Essen",
                        "45141"),
                    new DateTime(2006,02,09),
                    new []
                    {
                        new Artikel(
                            "Braun WK210 Sahara",
                            "Wasserkocher - kabellos",
                            30.00,
                            5),
                        new Artikel(
                            "Philips HP 4841 Compact",
                            "Haartrockner - kabelgebunden",
                            12.50,
                            2)
                    },
                    6.95
                );
            bestellung.ExportToXml(filename);
            Console.WriteLine("Exported Purchase Order");
            
            
            // Aufgabe 3
            Console.WriteLine("Aufgabe 3");
            filename = "outputMovies.xml";
            var movieArray = new MovieArray();
            movieArray.Add(new Movie(
                1,
                "Filmtitel 1",
                new Bewertung("IMDb", 8),
                "Komödie",
                new Sprache[]
                {
                    new Sprache(1, "deutsch"),
                    new Sprache(0, "englisch") 
                },
                "HD"));
            
            movieArray.Add(new Movie(
                2,
                "Filmtitel 2",
                new Bewertung("eigene", 1),
                "Thriller",
                null,
                "UHD"));
            movieArray.Serialize(filename);
            Console.WriteLine("Exported Movies");
        }
    }
}