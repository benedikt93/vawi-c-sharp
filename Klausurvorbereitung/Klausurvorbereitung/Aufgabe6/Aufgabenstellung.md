# Aufgabe 6: Vererbung
## a) (10 Punkte)
Erklären Sie den Unterschied zwischen abstrakten und virtuellen Methoden anhand einfacher Beispiele.
## b) (20 Punkte)
Erzeugen Sie eine Klasse Dimension, die die Verwaltung von graphischen Objekten erlaubt.

Die Basisklasse eignet sich zur Darstellung eines Rechtecks und hat zwei Properties, die die Ausdehnung in x- und y-Richtung
enthalten und eine Methode Fläche zur Berechnung des Flächeninhalts (x * y).

Die folgenden Klassen sollen ebenfalls entsprechend der Hierarchie implementiert werden:
```
             Recheck
                |
        ____________________
       |                    |
    Quadrat               Kreis
       |                    |
       |              ______________
       |             |              |
    Würfel         Kugel        Zylinder
```

Als Hilfestellung die Formeln zur Flächenberechnung der einzelnen Klassen:
- Rechteck (x*y)
- Quadrat (a2)
- Würfel (6*a2)
- Kreis (2πr2)
- Kugel (4πr2)
- Zylinder (2*r2*π + (2r*π*h))

Legen Sie ein Array vom Typ der Basisklasse (Dimension) an und erzeugen Sie hierbei Instanzen der Kindklassen. Geben Sie
anschließend den Flächeninhalt des Objekts auf der Konsole aus