﻿namespace Klausurvorbereitung.Aufgabe6
{
    public class Rechteck : Dimension
    {
        public int X { get; set; }
        public int Y { get; set; }

        public Rechteck(int x, int y)
        {
            X = x;
            Y = y;
        }

        public override double BerechneFlaeche()
        {
            return X * Y;
        }
    }
}