﻿using System;

namespace Klausurvorbereitung.Aufgabe10
{
    public class Aufgabe10Program
    {
        public static void Run()
        {
            var string1 = "hugo.mueller@mail-server.de";
            var string2 = "hugo.mueller@mail-server.de1344";
            var string3 = "+0049/12333";
            var string4 = "+049/1d2333";
            var string5 = "90409";
            var string6 = "9409";
            var string7 = "http://google.de/home.html";
            var string8 = "http://google/home.html";
            var string9 = "h.m@test.de keine-mail test@home.de 122 max.muster@uni-su.de";
            
            Console.WriteLine("String 1 is Mail: " + Validator.IsEmailAdress(string1));
            Console.WriteLine("String 2 is Mail: " + Validator.IsEmailAdress(string2));
            Console.WriteLine("String 3 is Phone: " + Validator.IsTelephoneNumber(string3));
            Console.WriteLine("String 4 is Phone: " + Validator.IsTelephoneNumber(string4));
            Console.WriteLine("String 5 is PLZ: " + Validator.IsPlz(string5));
            Console.WriteLine("String 6 is PLZ: " + Validator.IsPlz(string6));
            Console.WriteLine("String 7 is URL: " + Validator.IsUrl(string7));
            Console.WriteLine("String 8 is URL: " + Validator.IsUrl(string8));
            
            Console.WriteLine("Emails:");
            foreach (var match in Validator.ParseForEmailAddresses(string9))
            {
                Console.WriteLine(match);
            }
        }
    }
}