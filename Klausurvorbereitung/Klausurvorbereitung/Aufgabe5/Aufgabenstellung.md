# Aufgabe 5: Operatorüberladung
## a) (30 Punkte)
Gegeben ist eine Assembly *Geometrie.dll*, in der eine Klasse *Point* implementiert wurde. Die Klasse Point ist eine
einfache Implementierung eines Punktes im zweidimensionalen Raum. Ein Punkt wird durch seine zwei Koordinaten X und Y
repräsentiert, die als Properties vom Typ *int* in der Klasse *Point* implementiert wurden. Über diese Properties hinaus wurde
in der Klasse keine Funktionalität implementiert.

Gegeben sei des Weiteren das folgende Interface:
```c#
interface IDistance<T>
{
    double DistanceTo(T distantObject);
}
```
Implementieren Sie eine Klasse EnhancedPoint, die von der Klasse Point abgeleitet ist und um die folgenden Bestandteile erweitert wird:
- Erzeugen Sie geeignete Konstruktoren (Standard-Konstruktor, Copy-Konstruktor, Konstruktor mit x und y als Parameter)
- Sorgen Sie für eine ansprechende Ausgabe des Punktes im Konsolenfenster durch die in C# vorgesehenen Mechanismen.
- Implementieren Sie für diese Klasse die Operatoren + und – zur Verknüpfung von Punkten sowie den Operator ==.
- Implementieren Sie das Interface IDistance, um um die Entfernung zwischen zwei Punkten zu berechnen.
- Definieren Sie einen expliziten Cast, der die Entfernung eines Punkts vom Ursprung als float ausgibt.
Erstellen Sie ein Hauptprogramm, das alle Funktionen der Klasse ausreichend testet.

Hinweise:

Die Entfernung zweier Punkte berechnet sich nach der folgenden Formel

√(∆x)2 + (∆x)2

Die Methode Math.Sqrt gibt die Quadratwurzel einer angegeben Zahl zurück.

``public static double Sqrt(double d)``