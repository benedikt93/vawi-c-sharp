﻿using System;

namespace Klausurvorbereitung.Aufgabe6
{
    public class Kreis: Rechteck
    {
        public Kreis(int x) : base(x, x)
        {
        }

        public override double BerechneFlaeche()
        {
            return Math.PI * Math.Pow(X, 2);
        }
    }
}