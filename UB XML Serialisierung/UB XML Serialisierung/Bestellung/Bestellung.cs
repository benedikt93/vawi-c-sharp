﻿using System;
using System.IO;
using System.Linq;
using System.Xml.Serialization;

namespace UBXMLSerialisierung.Bestellung
{
    [XmlRoot("PurchaseOrder")]
    public class Bestellung
    {
        private XmlSerializer _xmlSerializer = new XmlSerializer(typeof(Bestellung));
        
        [XmlElement("ShipTo")]
        public Adresse LieferAdresse;

        [XmlIgnore]
        public DateTime Bestelldatum;

        [XmlElement("OrderDate")]
        public string FormatiertesBestellDatum
        {
            get => Bestelldatum.ToString("yyyy-MM-dd");
            set => Bestelldatum = DateTime.Parse(value);
        }
        
        [XmlArray("Items"), XmlArrayItem("OrderedItem")]
        public Artikel[] BestellArtikel;

        [XmlElement("SubTotal")]
        public double SummeArtikelKosten
        {
            get => BestellArtikel.Sum(a => a.Summe);
            set { }
        }
        
        [XmlElement("ShipCost")]
        public double Versandkosten;

        [XmlElement("TotalCost")]
        public double Gesamtkosten
        {
            get => SummeArtikelKosten + Versandkosten;
            set { }
        }

        public Bestellung(){}
        public Bestellung(Adresse lieferAdresse, DateTime bestelldatum, Artikel[] bestellArtikel, double versandkosten)
        {
            LieferAdresse = lieferAdresse;
            Bestelldatum = bestelldatum;
            BestellArtikel = bestellArtikel;
            Versandkosten = versandkosten;
        }

        public void ExportToXml(string fileName)
        {
            using (var streamWriter = new StreamWriter(fileName))
            {
                _xmlSerializer.Serialize(streamWriter, this);
            }
        }

        public Bestellung ImportFromXml(string fileName)
        {
            using (var streamWriter = new StreamReader(fileName))
            {
                return (Bestellung) _xmlSerializer.Deserialize(streamWriter);
            }
        }
    }
}