# Aufgabe 10: Zeichenkettenverarbeitung
## b) (20 Punkte)

Zum Kontext: Eine Assembly wird von einem Hauptprogramm verwendet (dieses muss **nicht** implementiert werden). Das
Hauptprogramm nutzt diese Assembly zum vaildieren von Benutzereingaben in einer Webanwendung. Die Validierung soll innerhalb der Assembly über Reguläre Ausdrücke erfolgen. Um die Komponente einfach nutzbar zu machen, soll nach außen
nichts von der internen Technologie sichtbar sein.

Implementieren Sie unter Verwendung regulärer Ausdrücke die beschriebene Assembly, die folgende Merkmale besitzt:
Implementieren Sie unter Verwendung regulärer Ausdrücke diese Assembly, die folgende Merkmale besitzt:
 - Die Assembly enthält eine Klasse *„Validator“*
 - Die Klasse *Validator* enthält die folgenden statischen Methoden, deren Parameter die zu prüfende Zeichenkette ist
und deren Rückgabewerte den Typ Boolean besitzen, um anzuzeigen, ob der Paramterausdruck dem gewünschten Schema
entspricht.
 - *IsEmailAddress*

   Schema: z.B. hugo.mueller@mail-server.de
 - *IsTelephoneNumber*

   Schema: deutsche Telefonnummer inkl. Vorwahl durch „/“ getrennt.
 - *IsPlz*
   
   Schema: Deutsche Postleitzahl mit dem optionalen Präfix „D-„
 - *IsUrl*
 
   Schema: http://host-name.sub-domain.domain. topleveldomain/ directory/file.html

Zusätzlich soll eine weitere Methode *ParseForEmailAddresses* implementiert werden, die aus einer beliebigen gegebenen Zeichenkette Emailadressen herausfiltert und zurückgibt. Wenn in der Zeichenkette mehr als ein Treffer enthalten ist,
sollen entsprechend mehrere Ergebnisse (Emailadressen) zurückgegeben werden.

Parameter und Rückgabewerte sind vom Typ *string* bzw. *string[]*.

Geben Sie in jeder Methode innerhalb eines Kommentars mindestens je ein Beispiel für einen erfolgreichen und einen erfolglosen Treffer an.

Als Hilfestellung sind die folgenden Sprachbestandteile regulärer
Ausdrücke gegeben:

| | Zeichen|
| ---|---|
| . | Beliebiges Zeichen außer \n |
| \n | New Line| 
| \d | Dezimal-Zeichen| 
| \s | White-Space-Zeichen| 
| \w | Wortzeichen| 
| | Quantifizierer| 
| * | 0-n| 
| + | 1-n| 
| ? | 0-1| 
| {n} | N| 
