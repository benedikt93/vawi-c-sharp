﻿namespace Klausurvorbereitung.Aufgabe6
{
    public abstract class Dimension
    {
        public abstract double BerechneFlaeche();
    }
}