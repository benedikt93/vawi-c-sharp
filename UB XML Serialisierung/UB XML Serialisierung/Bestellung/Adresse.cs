﻿using System.Xml.Serialization;

namespace UBXMLSerialisierung.Bestellung
{
    public class Adresse
    {
        [XmlAttribute("Forename")]
        public string Vorname;
        
        [XmlAttribute("Surname")]
        public string Nachname;
        
        [XmlElement("Street")]
        public string Strasse;
        
        [XmlElement("City")]
        public string Stadt;
        
        [XmlElement("Zip")]
        public string PLZ;

        public Adresse(){}
        public Adresse(string vorname, string nachname, string strasse, string stadt, string plz)
        {
            Vorname = vorname;
            Nachname = nachname;
            Strasse = strasse;
            Stadt = stadt;
            PLZ = plz;
        }
    }
}