﻿namespace VideoStore
{
    public class Movie
    {
        public enum Type
        {
            CHILDRENS = 2,
            REGULAR = 0,
            NEW_RELEASE = 1
        };

        private string title;
        private Price _price;

        public Movie(string title, Type priceCode)
        {
            this.title = title;
            PriceCode = priceCode;
        }

        public Type PriceCode
        {
            get { return _price.GetPriceCode(); }
            set
            {
                switch (value)
                {    
                    case Type.REGULAR:
                        _price = new RegularPrice();
                        break;
                    case Type.NEW_RELEASE:
                        _price = new NewReleasePrice();
                        break;
                    case Type.CHILDRENS:
                        _price = new ChildrenPrice();
                        break;
                }
            }
        }

        public string Title
        {
            get { return title; }
        }

        public double GetCharge(int daysRent) =>  _price.GetCharge(daysRent);

        public int GetFrequentRenterPoints(int daysRent) => _price.GetFrequentRenterPoints(daysRent);
    }
}