# Aufgabe 27: Generics
## a) (5 Punkte)
Nennen Sie drei Vorteile von Generics.
## b) (10 Punkte)
Notieren Sei ein kurzes Code-Snippet, das den Einsatz von Generics anhand einer Collection aus den Basisklassen veranschaulicht und sowohl
- Deklaration,
- Instanziierung der Liste,
- Hinzufügen von Elementen und
- den Aufruf einer Methode eines Elements aus der Liste

umfasst. Wählen Sie für das Beispiel einen geeigneten Typ als Element aus
## c) (15 Punkte)
Implementieren Sie eine Assembly, die eine generische Klasse DocumentManager enthält. Die Klasse DocumentManager soll
über die Mechanismen der Generics Dokumente unterschiedlichen Typs in einer Collection verwalten können. Als öffentliche
Member sollen
- eine Add-Methode zum Hinzufügen von Dokumenten
- eine Get-Methode zum Auslesen von Dokumenten
- eine IsDocumentAvaillable, die false zurückgibt, wenn die Collection leer ist (und umgekehrt)

implementiert werden.