﻿using System.Xml.Serialization;

namespace UBXMLSerialisierung.Movies
{
    public class Movie
    {
        [XmlAttribute("id")]
        public int Id;
        public string Filmnane;
        public Bewertung Bewertung;
        public string Genre;
        
        [XmlArrayItem("Sprache")]
        public Sprache[] Sprachen;
        public string Format;

        public Movie()
        {
        }

        public Movie(int id, string filmnane, Bewertung bewertung, string genre, Sprache[] sprachen, string format)
        {
            Id = id;
            Filmnane = filmnane;
            Bewertung = bewertung;
            Genre = genre;
            Sprachen = sprachen;
            Format = format;
        }
    }
}