﻿namespace Klausurvorbereitung.Aufgabe6
{
    public class Wuerfel: Quadrat
    {
        public Wuerfel(int x) : base(x)
        {
        }

        public override double BerechneFlaeche() => 6 * base.BerechneFlaeche();
    }
}