# UB 13 Refactoring
## Qualifikationsziele
Die Studierenden können die Maßnahmen des Refaktorisierens praktisch einsetzen

## Rechnungsstellung einer Videothek
Aufgabe dieser Übungseinheit ist es, die Refaktorisierungsschritte des
Beispiels vollständig durchzuführen. Gegeben ist der Ausgangszustand in
C#.
1. Konfigurieren Sie Ihre Entwicklungsumgebung so, dass Sie Unit Tests
verwenden können.
2. Führen Sie den Test im Ausgangsprojekt aus und ergänzen Sie einen
weiteren Test.
3. Arbeiten Sie die Schritte aus dem Beispiel vollständig durch, bis ihr
Projekt dem Ziel-Design entspricht. (Beachten Sie bei der Umsetzung,
dass das Beispiel in Java vorliegt und kleinere Abweichungen zu C#
vorhanden sein können.)
