# Aufgabe 9: Zeichenkettenverarbeitung
## b) (20 Punkte)

Zum Kontext: Eine Assembly wird von einem Hauptprogramm verwendet
(dieses muss nicht implementiert werden),
das Datensätze aus einer Datei zeilenweise einliest.
Das Hauptprogramm übergibt jede Zeile der Reihe nach an die Methoden der Assembly und verarbeitet das Ergebnis weiter.
Als Ergebnis wird bei den spezifischen Methoden jeweils nur ein Treffer erwartet.

Implementieren Sie unter Verwendung regulärer Ausdrücke diese Assembly, die folgende Merkmale besitzt:
- Die Assembly enthält eine Klasse „AdressParser“
- Die Klasse AdressParser enthält die folgenden statischen Methoden, deren Parameter und Rückgabewert den Typ string
besitzen. Die Methoden geben jeweils das durch den Methodennamen spezifizierte Element zurück.
- SearchStreet // Straße inkl. Hausnummer
- SearchLastName // Nachname
- SearchBirthDate // Geburtsdatum
- Der Eingabestring, der für alle Methoden identisch ist, entspricht dem folgenden Aufbau:

 ```
 Name;Adresse;Geburtsdatum
 ```
     
- Beispiele, mit denen die Assembly korrekt arbeiten soll:
```
Meier, Hans;Heinrichstr. 34 45332 Essen;23.02.2001
Müller-Lüdenscheidt, Otto;Berliner Str. 89 45123 Essen;12.04.1987
Schmidt, Hans O.;Sommer Allee 1 17643 Lubmin;01.03.1917
```
- Zusätzlich ist gewünscht, dass ein fortgeschrittener Anwender über eine weitere Methode unter Angabe von Datenzeile
und Suchpattern weitere Elemente aus den Textzeilen ermitteln kann. Daher soll eine allgemeine Methode bereitgestellt
werden, die den Suchtext und das Pattern übergeben bekommt und mögliche Suchergebnisse (Plural!) als Rückgabewert
liefert.

Als Hilfestellung sind die folgenden Sprachbestandteile regulärer Ausdrücke gegeben:

| | Zeichen|
| ---|---|
| . | Beliebiges Zeichen außer \n |
| \n | New Line| 
| \d | Dezimal-Zeichen| 
| \s | White-Space-Zeichen| 
| \w | Wortzeichen| 
| | Quantifizierer| 
| * | 0-n| 
| + | 1-n| 
| ? | 0-1| 
| {n} | N| 
