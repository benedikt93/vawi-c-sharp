﻿using System.Linq;

namespace Klausurvorbereitung.Aufgabe12
{
    public class Order
    {
        public double OrderPrice { get; set; }
    }

    public class Cutomer
    {
        public string Forename { get; set; }
        public string Surname { get; set; }
        public string TelephoneNumer { get; set; }
        public string Email { get; set; }
        public Order[] OrderList { get; set; }
        public double SumOfAllOrders => OrderList.Sum(o => o.OrderPrice);

        public override string ToString()
            => $"{Surname}, {Forename} (Tel: {TelephoneNumer})";
    }
}