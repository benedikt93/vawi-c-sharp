﻿using System.Xml.Serialization;

namespace UBXMLSerialisierung.Movies
{
    public class Sprache
    {
        [XmlAttribute("untertiel")]
        public int NumSubtitle;
        
        [XmlText]
        public string Language;

        public Sprache()
        {
        }

        public Sprache(int numSubtitle, string language)
        {
            NumSubtitle = numSubtitle;
            Language = language;
        }
    }
}