﻿using System;

namespace Klausurvorbereitung.Aufgabe3
{
    public static class Aufgabe3Program
    {
        public static void run()
        {
            Vorlesungsteilnehmer cSharp = new Vorlesungsteilnehmer(3);
            cSharp.Add(new Student("Beate", '1'));
            var bene = new Student("Bene", '0');
            cSharp.Add(bene);
            Console.WriteLine("Nach dem init:");
            Console.WriteLine("Expect Count to be 2 and is: " + cSharp.Count );
            Console.WriteLine("Expect Female Count to be 1 and is: " + cSharp.CountFemale );
            Console.WriteLine("Expect Male Count to be 1 and is: " + cSharp.CountMale );
            
            cSharp.Add(new Student("Jörg", '1'));
            Console.WriteLine("Nach dem 2. Hinzufügen:");
            Console.WriteLine("Expect Count to be 3 and is: " + cSharp.Count );
            Console.WriteLine("Expect Female Count to be 2 and is: " + cSharp.CountFemale );
            Console.WriteLine("Expect Male Count to be 1 and is: " + cSharp.CountMale );
            
            cSharp.Add(new Student("Geht ja eh ned", '0'));
            Console.WriteLine("Nach dem unerfolgreichen Hinzufügen:");
            Console.WriteLine("Expect Count to be 3 and is: " + cSharp.Count );
            Console.WriteLine("Expect Female Count to be 2 and is: " + cSharp.CountFemale );
            Console.WriteLine("Expect Male Count to be 1 and is: " + cSharp.CountMale );
            
            cSharp.Remove(bene);
            Console.WriteLine("Nach dem entfernen:");
            Console.WriteLine("Expect Count to be 2 and is: " + cSharp.Count );
            Console.WriteLine("Expect Female Count to be 2 and is: " + cSharp.CountFemale );
            Console.WriteLine("Expect Male Count to be 0 and is: " + cSharp.CountMale );

            Console.Read();
        }
    }
}