﻿using System.Collections;
using System.Linq;

namespace VideoStore
{
    public class Customer
    {
        private string name;
        private ArrayList rentals = new ArrayList();

        public Customer(string name)
        {
            this.name = name;
        }

        public void AddRental(Rental arg)
        {
            rentals.Add(arg);
        }

        public string Name
        {
            get { return name; }
        }

        public string Statement()
        {
            string result = "Rental Record for " + Name + "\n";
            foreach (Rental aRental in rentals)
            {
                // Zahlen für diese Ausleihe ausgeben
                result += "\t" + aRental.Movie.Title + "\t" +
                          aRental.GetCharge() + "\n";
            }
            // Fußzeilen einfügen
            result += "Amount owed is " + GetTotalCharge() + "\n";
            result += "You earned " + GetTotalFrequentRenterPoints() +
                      " frequent renter points";
            return result;
        }
        
        public string HtmlStatement()
        {
            string result = "<h1>Rental Record for <em>" + Name + "</em></h1>";
            foreach (Rental aRental in rentals)
            {
                // Zahlen für diese Ausleihe ausgeben
                result += "<p>" + aRental.Movie.Title + ": " +
                          aRental.GetCharge() + "</p>\n";
            }
            // Fußzeilen einfügen
            result += "<p> You owe <em>" + GetTotalCharge() + "</em></p>\n";
            result += "<p> On this rental you earned <em>" + GetTotalFrequentRenterPoints() +
                      "</em> frequent renter points </p>";
            return result;
        }
        
        private double GetTotalCharge() =>
            rentals.Cast<Rental>().Sum(r => r.GetCharge());
        
        private double GetTotalFrequentRenterPoints() => 
            rentals.Cast<Rental>().Sum(rental => rental.GetFrequentRenterPoints());
    }
}