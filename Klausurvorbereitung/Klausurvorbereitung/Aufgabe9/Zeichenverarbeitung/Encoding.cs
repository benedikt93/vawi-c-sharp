﻿using System.Text;

namespace Klausurvorbereitung.Aufgabe9.Zeichenverarbeitung
{
    public class Encoding
    {
        public string Encode(string text)
        {
            StringBuilder result = new StringBuilder(text);
            
            for (int i = (int) 'z'; i >= (int) 'a'; i--)
            {
                char alt = (char) i;
                char neu = (char) (i + 2);
                result.Replace(alt, neu);
            }
            
            for (int i = (int) 'Z'; i >= (int) 'A'; i--)
            {
                char alt = (char) i;
                char neu = (char) (i + 2);
                result.Replace(alt, neu);
            }

            return result.ToString();
        }
    }
}