﻿namespace Klausurvorbereitung.Aufgabe3
{
    public class Student
    {
        public string Name;
        public char Geschlecht;

        public Student(string name, char geschlecht)
        {
            Name = name;
            Geschlecht = geschlecht;
        }
    }
}