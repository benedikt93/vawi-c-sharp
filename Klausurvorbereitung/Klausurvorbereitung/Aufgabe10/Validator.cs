﻿using System;
using System.Collections;
using System.Text.RegularExpressions;

namespace Klausurvorbereitung.Aufgabe10
{
    public class Validator
    {
        private static string _mailRegex = @"\w+\.?\w+\@[\w-]+(\.\w+)?\.\w{2,4}";
        private static string _phoneRegex = @"^\+?\d+\/?\d+$";
        private static string _plzRegex = @"^\d{5}$";
        private static string _urlRegex = @"^https?:\/{2}\w+(\-\w+)*\.([\w-]+.)*\w+(\/\w+)*(\.html)?$";
        
        
        public static bool IsEmailAdress(string input)
        {
            return Regex.IsMatch(input, _mailRegex);
        }

        public static bool IsTelephoneNumber(string input)
        {
            return Regex.IsMatch(input, _phoneRegex);
        }

        public static bool IsPlz(string input)
        {
            return Regex.IsMatch(input, _plzRegex);
        }

        public static bool IsUrl(string input)
        {
            return Regex.IsMatch(input, _urlRegex);
        }

        public static string[] ParseForEmailAddresses(string input)
        {
            ArrayList result = new ArrayList();
            foreach (var match in Regex.Matches(input, _mailRegex))
            {
                result.Add(match.ToString());
            }
            return (string[]) result.ToArray(typeof(string));
        }
    }
}