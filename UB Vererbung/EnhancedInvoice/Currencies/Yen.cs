﻿namespace EnhancedInvoice.Currencies
{
    public class Yen : BaseCurrency
    {
        public Yen(double value) : base(value, ECurrency.Yen)
        {
        }
    }
}