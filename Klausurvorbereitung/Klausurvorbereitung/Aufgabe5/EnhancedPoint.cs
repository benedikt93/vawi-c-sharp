﻿using System;

namespace Klausurvorbereitung.Aufgabe5
{
    public class EnhancedPoint : Point, iDistance<Point>
    {
        // Konstruktoren
        
        public EnhancedPoint(){}
        
        public EnhancedPoint(int x, int y)
        {
            X = x;
            Y = y;
        }
        
        public EnhancedPoint(Point point): this(point.X, point.Y){}

        // Operatoren
        
        public static EnhancedPoint operator +(EnhancedPoint p1, EnhancedPoint p2)
            => new EnhancedPoint(p1.X + p2.X, p1.Y + p2.Y);
        
        public static Point operator -(EnhancedPoint p1, EnhancedPoint p2)
            => new EnhancedPoint(p1.X - p2.X, p1.Y - p2.Y);

        public static bool operator ==(EnhancedPoint p1, EnhancedPoint p2)
            => p1.X == p2.X && p1.Y == p2.Y;

        public static bool operator !=(EnhancedPoint p1, EnhancedPoint p2) => !(p1 == p2);

        public static explicit operator float(EnhancedPoint p)
            => (float) p.DistanceTo(new EnhancedPoint(0, 0));
        
        // Methoden
        
        public override string ToString() => $"Koordinaten: ({X}, {Y})";
        
        public double DistanceTo(Point distantObject)
            => Math.Sqrt(Math.Pow(X - distantObject.X, 2) + Math.Pow(Y - distantObject.Y, 2));
        
    }
}