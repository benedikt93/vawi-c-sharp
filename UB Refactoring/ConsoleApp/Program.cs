﻿using System;

namespace VideoStore
{
    class Program
    {
        static void Main(string[] args)
        {
            Movie m1 = new Movie("Die fabelhafte Welt der Amelie", Movie.Type.REGULAR);
            Movie m2 = new Movie("Titanic 2", Movie.Type.NEW_RELEASE);
            Movie m3 = new Movie("Ferkels großes Abenteuer", Movie.Type.CHILDRENS);
            
            Customer c1 = new Customer("Peter M. Schuler");

            Rental r1 = new Rental(m1, 12);
            Rental r2 = new Rental(m2, 3);
            Rental r3 = new Rental(m3, 5);
            
            c1.AddRental(r1);
            c1.AddRental(r2);
            c1.AddRental(r3);

            Console.WriteLine(c1.Statement());
            Console.WriteLine("----------------------");
            Console.WriteLine(c1.HtmlStatement());
        }
    }
}
