﻿using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace ParserLibrary
{
    public class AddressParser
    {
        public static List<string> convertFileContent(List<string> input)
        {
            var result = new List<string>();

            const string lastName = @"[\S]+$";
            const string firstName = @".+\s";
            const string street = @".+[^\d]";
            const string houseNumber = @"\d+";

            foreach (var line in input)
            {
                var parts = line.Split('|');
                var entries = new List<string>
                {
                    Regex.Match(parts[0], lastName).ToString(),
                    Regex.Match(parts[0], firstName).ToString().Trim(),
                    Regex.Match(parts[1], street).ToString().Trim(),
                    Regex.Match(parts[1], houseNumber).ToString().Trim(),
                    parts[4],
                    parts[2],
                    parts[3]
                };
                result.Add(string.Join(", ", entries.ToArray()));
            }
            return result;
        }
    }
}