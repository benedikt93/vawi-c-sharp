﻿using System.Numerics;

namespace EnhancedInvoice.Currencies
{
    public class Dollar : BaseCurrency
    {
        public Dollar(double value) : base(value, ECurrency.Dollar) {}
        
        public static implicit operator Dollar(Yen yen) => new Dollar(yen.Value / 10);
        public static implicit operator Dollar(Euro euro) => new Dollar(euro.Value / 2);
        
        public static Dollar operator +(Dollar c1, Dollar c2)
        {
            return new Dollar( c1.Value + c2.Value);  
        }
    }
}