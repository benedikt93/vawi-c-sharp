﻿namespace Klausurvorbereitung.Aufgabe6
{
    public class Kugel: Kreis
    {
        public Kugel(int x) : base(x)
        {
        }

        public override double BerechneFlaeche()
        {
            return base.BerechneFlaeche() * 4;
        }
    }
}