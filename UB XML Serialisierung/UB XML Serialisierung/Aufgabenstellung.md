# UB 10 XML Serialisierung
## Qualifikationsziele
Die Studierenden können die Serialisierung und Deserialisierung eines Objekts
durchführen und entsprechend der Anforderungen anpassen

## Aufgabe 1
### Teilaufgabe 1
Implementieren Sie eine *Assembly*, die eine Bibliothek abbildet.

Innerhalb des Namespaces *Library* sollen
- die Klassen *Book* und *Inventory* sowie die
- Enumeration *BookGenre* mit den Konstanten Sachbuch und Roman

Implementiert werden.

Beachten Sie bei der Implementierung die weiteren Teilaufgaben,
damit Sie die Anforderungen frühzeitig bedenken oder
entsprechenden Freiraum lassen.

Für die Klassen gelten die folgenden Anforderungen:
- Die Klasse Book enthält die folgenden Properties
  - Title vom Typ string
  - Author vom Typ string
  - Genre vom Typ BookGenre
  - Year vom Typ int
  - ISBN vom Typ string
  - Price vom Typ decimal
  
- Die Klasse Inventory enthält die
folgenden Properties
  - Books vom Typ Array von Book
  - TotalValue vom Typ decimal
zur Berechnung des Gesamtwertes
aller Bücher in der Bibliothek
(Bücher in Books)

- Implementieren Sie für die Klasse
Inventory eine Überschreibung der
ToString()-Methode, die die Titel
(Title) aller Bücher in Books sowie
den Gesamtwert (Total-Value)
ausgibt

### Teilaufgabe 2
Implementieren Sie eine zusätzliche Klasse Serializer im Namespace
Library, die eine Serialisierung bzw. Deserialisierung der Klasse
Inventory ermöglicht.
- Die Klasse soll dazu zwei Methoden zur Verfügung stellen:
  - public void ExportToXml(string fileName, Inventory
inventory)
  - public Inventory ImportFromXml(string fileName)
- Nutzen Sie XML-Attribute um zu erreichen, dass die Serialisierung dem
im Folgenden abgebildeten Ergebnis entspricht (Abbildung 1 und
Abbildung 2).
- In der Tabelle können Sie hierzu mögliche Attribute nachschlagen
(Tabelle 1).

**Abbildung 1 – Format der XML-Datei ohne den Einsatz von Attributen**
```
<?xml version="1.0" encoding="utf-8"?>
<Inventory xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
xmlns:xsd="http://www.w3.org/2001/XMLSchema">
 <Books>
  <Book>
   <Title>C# 2012 and .NET 4.5</Title>
   <Author>Christian Nagel et al.</Author>
   <Genre>Sachbuch</Genre>
   <Year>2013</Year>
   <ISBN>978-1-118-31442-5</ISBN>
   <Price>50</Price>
  </Book>
 </Books>
 <TotalValue>50</TotalValue>
</Inventory>
```
**Abbildung 2 – Zielformat der XML-Datei mit Attributen**
```
<?xml version="1.0" encoding="utf-8"?>
<Inventory xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
xmlns:xsd="http://www.w3.org/2001/XMLSchema">
 <Books>
  <Book Title="C# 2012 and .NET 4.5" Author="Christian Nagel et al.">
   <Genre>Sachbuch</Genre>
   <Year>2013</Year>
   <ISBN>978-1-118-31442-5</ISBN>
   <Price>50</Price>
  </Book>
 </Books>
 <TotalValue>50</TotalValue>
</Inventory>
```

## Aufgabe 2
- Implementieren Sie eine Assembly, die das
Pendant zum abgebildeten XML-Dokument bildet.
Das XML-Dokument entspricht einem
Austauschformat mit einem internationalen
Parterunternehmen. Im Gegensatz zum
englischsprachigen XML-Dokument erfolgt die
Implementierung unter Berücksichtigung der
unternehmensinternen Namensgebung, die
deutsche Bezeichner für Klassen, Felder, etc.
vorschreibt.

- Berücksichtigen Sie bei der Implementierung die
Einordnung in einen geeigneten Namespace, eine
geeignete Bezeichnung der Felder, Klassen, etc.
Treffen Sie für komplexe Elemente der XMLStruktur
entsprechende
Implementierungsentscheidungen, um eine
möglichst hohe Deckung zwischen XML-Struktur
und Klasse zu erreichen.

- Beachten Sie bei der Implementierung, dass einige
Einträge innerhalb einer Klasse als Methoden bzw.
Properties bereitgestellt werden und dynamisch
berechnet werden sollen. Treffen Sie angemessene
Entscheidungen bzgl. der Datentypen. Es ist nicht
erforderlich, einfache Felder als Properties zu
kapseln.
  - Die Klasse soll zwei Methoden bereitstellen, die
die Serialisierung und Deserialisierung kapseln.
  - ExportToXml(string fileName)
  - ImportFromXml(string fileName)
  
- Nutzen Sie XML-Attribute um zu
  erreichen, dass die Serialisierung
  dem abgebildeten Ergebnis
  entspricht. In der Tabelle können
  Sie hierzu mögliche Attribute
  nachschlagen.
  
## Aufgabe 3
Implementieren Sie ein Klassenkonstrukt, das der folgenden XMLDatei
gerecht wird.
```
<?xml version="1.0" encoding="utf-8"?>
<ArrayOfMovie xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
xmlns:xsd="http://www.w3.org/2001/XMLSchema">
  <Movie id="1">
    <Filmname>Filmtitel 1</Filmname>
    <Bewertung typ="IMDb">8</Bewertung>
    <Genre>Komödie</Genre>
    <Sprachen>
      <Sprache untertitel="1">deutsch</Sprache>
      <Sprache untertitel="0">englisch</Sprache>
    </Sprachen>
    <Format>HD</Format>
  </Movie>
  <Movie id="2">
    <Filmname>Filmtitel 2</Filmname>
    <Bewertung typ="eigene">1</Bewertung>
    <Genre>Thriller</Genre>
    <Format>UHD</Format>
  </Movie>
</ArrayOfMovie>
```