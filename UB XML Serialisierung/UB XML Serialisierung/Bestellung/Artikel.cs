﻿using System.Xml.Serialization;

namespace UBXMLSerialisierung.Bestellung
{
    public class Artikel
    {
        [XmlElement("ItemName")]
        public string Artikelname;
        
        [XmlElement("Description")]
        public string Beschreibung;
        
        [XmlElement("UnitPrice")]
        public double Einzelpreis;
        
        [XmlElement("Quantity")]
        public int Anzahl;

        public Artikel(){}
        public Artikel(string artikelname, string beschreibung, double einzelpreis, int anzahl)
        {
            Artikelname = artikelname;
            Beschreibung = beschreibung;
            Einzelpreis = einzelpreis;
            Anzahl = anzahl;
        }

        [XmlElement("LineTotal")]
        public double Summe
        {
            get => Einzelpreis * Anzahl;
            set { }
        }
    }
}