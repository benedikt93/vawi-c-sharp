﻿using System.Xml.Serialization;

namespace UBXMLSerialisierung.Movies
{
    public class Bewertung
    {
        [XmlAttribute("typ")]
        public string Type;
        [XmlText]
        public int Value;

        public Bewertung(string type, int value)
        {
            Type = type;
            Value = value;
        }

        public Bewertung()
        {
        }
    }
}