﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

namespace UBXMLSerialisierung.Movies
{
    [XmlRoot("ArrayOfMovie")]
    [XmlInclude(typeof(Movie))]
    public class MovieArray : List<Movie>
    {
        private XmlSerializer _xmlSerisalizer = new XmlSerializer(typeof(MovieArray));

        public void Serialize(string fileName)
        {
            using (var streamWriter = new StreamWriter(fileName))
            {
                _xmlSerisalizer.Serialize(streamWriter, this);
            }
        }
    }
}