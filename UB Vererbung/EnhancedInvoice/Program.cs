﻿using Financials;
using System;
using EnhancedInvoice.Currencies;

namespace EnhancedInvoice
{
    class Program
    {
        //static void Main(string[] args)
        //{
        //    Invoice myInvoice;
        //    InvoiceItem myItem;

        //    myInvoice = new Invoice();
        //    myInvoice.Date = System.DateTime.Today;
        //    myInvoice.Number = 1234;
        //    myInvoice.Currency = "EUR";
        //    myInvoice.Recipient = "Herr Meier";

        //    myItem = new InvoiceItem();
        //    myItem.Stocknumber = 5678;
        //    myItem.Description = "Kaffetasse, groß";
        //    myItem.Quantity = 23;
        //    myItem.UnitPrice = 12.99;
        //    myInvoice.AddItem(myItem);

        //    myItem = new InvoiceItem();
        //    myItem.Stocknumber = 7856;
        //    myItem.Description = "Kaffetasse, klein";
        //    myItem.Quantity = 12;
        //    myItem.UnitPrice = 9.99;
        //    myInvoice.AddItem(myItem);

        //    Console.WriteLine(((InvoiceItem)myInvoice.Items[0]).SumPrice);
        //    Console.ReadLine(); 

        //}


    }

    class ProgramEnhanced
    {
        static void Main(string[] args)
        {
            EnhancedInvoice myInvoice;

            myInvoice = new EnhancedInvoice(System.DateTime.Today, 1234, "EUR", "Herr Meier");
            myInvoice.AddItem(new EnhancedInvoiceItem(5678, 23, "Kaffetasse, groß", 12.99));
            myInvoice.AddItem(new EnhancedInvoiceItem(7856, 12, "Kaffetasse, klein", 9.99));

            Invoice myBase = myInvoice;

            // Aufruf durch das überschriebene Property
            Console.WriteLine(myInvoice.Items[1].SumPrice);

            // Aufruf durch den Indexer
            Console.WriteLine(myInvoice[1].SumPrice);

            // Ausgabe über ToString
            Console.WriteLine(myInvoice.ToString());
            Console.WriteLine(myBase.ToString());
            
            // Aufgabe 7 Ausgabe aller Items
            Console.WriteLine("Aufgabe 7: Alle Items mittels Foreach:");
            
            foreach (EnhancedInvoiceItem eit in myInvoice)
            {
                Console.WriteLine(eit.ToString());
            }
            
            // Aufgabe 7 Casts
            Console.WriteLine("Aufgabe 7: Währungen umrechnen:");
            var dol = new Dollar(7);
            Euro eur = (Euro)dol;
            Console.WriteLine("7 Dollar sind " + eur + " Euro");
            
            var yen = new Yen(100);
            Dollar dol2 = yen;
            Console.WriteLine("100 Yen sind " + dol2 + " Dollar");
            
            var dol3 = new Dollar(3);
            var dol4 = new Dollar(5);
            var dolSum = dol3 + dol4;
            Console.WriteLine(dolSum);
            
            var euro5 = new Euro(4);
            var yen6 = new Yen(200);
            Dollar dolSum2 = (Dollar)euro5 + yen6; // Hier noch expliziter cast notwendig, weil ich nicht so schlau bin
            Console.WriteLine("Expect " + dolSum2 + " to be 22");

            
            Console.ReadKey();
        }
    }
}
