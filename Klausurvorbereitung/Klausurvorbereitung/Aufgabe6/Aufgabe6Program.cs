﻿using System;

namespace Klausurvorbereitung.Aufgabe6
 {
     public class Aufgabe6Program
     {
         public static void Run()
         {
             var dimArray = new Dimension[]
             {
                 new Rechteck(2, 3),
                 new Quadrat(2), 
                 new Wuerfel(4),
                 new Kreis(2),
                 new Kugel(3),
                 new Zylinder(2, 3) 
             };

             foreach (var dimension in dimArray)
             {
                 Console.WriteLine("Fläche: " + dimension.BerechneFlaeche());
             }
         }
     }
 }