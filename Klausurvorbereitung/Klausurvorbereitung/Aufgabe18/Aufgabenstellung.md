# Aufgabe 20: XML-Serialisierung
## a) (10 Punkte)
Implementieren Sie eine Assembly, die einen Bestellauftrag abbildet.

Innerhalb des Namespaces *Finance* sollen die Klassen *PurchaseOrder*, *Address* und *OrderItem* implementiert
werden. Beachten Sie bei der Implementierung die zweite Teilaufgabe, damit Sie die Anforderungen frühzeitig bedenken oder
vor den jeweiligen Elementen entsprechend eine Zeile frei lassen.

Für die Klassen gelten die folgenden Anforderungen:
- Die Klasse Address ist gegeben und muss nicht implementiert werden. Sie enthält die folgenden Properties vom Typ
string
- Name, Street, Zip, City
- Die Klasse *OrderItem* enthält die folgenden **Properties**
   - ItemName vom Typ string
   - Description vom Typ string
   - UnitPrice vom Typ decimal
   - Quantity vom Typ int
   - LineTotal vom Typ decimal (Berechnung des Gesamtpreises einer Zeile)
- Die Klasse *PurchaseOrder* enthält die folgenden **Properties**
   - ShipTo vom Typ Address
   - OrderDate vom Typ string
   - OrderedItems vom Typ Array von OrderItem
   - SubTotal vom Typ decimal (Summe der Preise der bestellten Artikel)
   - ShipCost vom Typ decimal
   - TotalCost vom Typ decimal (Summe SubTotal + ShipCost)
   
## b) (15 Punkte)
Gegeben sei folgendes Interface (aus dem Namespace *Persistence*) zur Serialisierung bzw. Deserialisierung von Objekten
in eine Datei fileName bzw. umgekehrt.
````c#
interface IXmlFileSerializer<T>
{
    void ExportToXml(string fileName);
    T ImportFromXml(string fileName);
}
````
Implementieren Sie innerhalb der Klasse PurchaseOrder dieses Interface.

Passen Sie zusätzlich die Implementierung der Klasse an, damit die Ausgabe des XML-Dokumentes der Abbildung 1 entspricht.
Ohne den Einsatz von Attributen entspricht die Ausgabe der Abbildung 2. In der Tabelle können Sie mögliche Attribute nachschlagen.
````xml
<?xml version="1.0" encoding="utf-8"?>
<PurchaseOrder xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:xsd="http://www.w3.org/2001/XMLSchema" OrderDate="2014-02-10">
    <ShipTo Name="Hugo Müller" Street="Zum Irrtum 12" ZIP="37121" City="Velbert" />
    <Items>
        <Item ItemName="iPad" Description="Tablet" UnitPrice="899.99" Quantity="1"/>
        <Item ItemName="Rey Color A4 100" UnitPrice="12.99" Quantity="5" />
    </Items>
    <ShipCost>4.55</ShipCost>
</PurchaseOrder>
````
Abbildung 1 - Zielformat der XML-Datei
````xml
<?xml version="1.0" encoding="utf-8"?>
<PurchaseOrder xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <ShipTo Name="Hugo Müller" Street="Zum Irrtum 12" ZIP="37121" City="Velbert" />
    <OrderDate>2014-02-10</OrderDate>
    <OrderedItems>
        <OrderItem>
            <ItemName>iPad</ItemName>
            <Description>Tablet</Description>
            <UnitPrice>270.65</UnitPrice>
            <Quantity>1</Quantity>
        </OrderItem>
        <OrderItem>
            <ItemName>Rey Color A4 100</ItemName>
            <UnitPrice>8.99</UnitPrice>
            <Quantity>5</Quantity>
        </OrderItem>
    </OrderedItems>
    <ShipCost>4.55</ShipCost>
</PurchaseOrder>
````
Abbildung 2 - Format der XML-Datei ohne Einsatz von Attributen