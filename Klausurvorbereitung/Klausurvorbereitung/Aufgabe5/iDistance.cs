﻿namespace Klausurvorbereitung.Aufgabe5
{
    public interface iDistance<T>
    {
        double DistanceTo(T distantObject);
    }
}