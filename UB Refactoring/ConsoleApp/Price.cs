﻿namespace VideoStore
{
    public abstract class Price
    {
        public abstract Movie.Type GetPriceCode();

        public abstract double GetCharge(int daysRent);

        // hier virtual notwendig
        public virtual int GetFrequentRenterPoints(int daysRent) => 1;
    }

    public class ChildrenPrice : Price
    {
        public override Movie.Type GetPriceCode() => Movie.Type.CHILDRENS;

        public override double GetCharge(int daysRent) => 
            1.5 + (daysRent > 3 ? (daysRent - 3) * 1.5 : 0);
    }
    
    public class NewReleasePrice : Price
    {
        public override Movie.Type GetPriceCode() => Movie.Type.NEW_RELEASE;

        public override double GetCharge(int daysRent) => daysRent * 3;

        public override int GetFrequentRenterPoints(int daysRent) =>
            daysRent > 1 ? 2 : base.GetFrequentRenterPoints(daysRent);
    }
    
    public class RegularPrice : Price
    {
        public override Movie.Type GetPriceCode() => Movie.Type.REGULAR;
        
        public override double GetCharge(int daysRent) =>
            2 + (daysRent > 2 ? (daysRent - 2) * 1.5 : 0);
    }
}