﻿namespace EnhancedInvoice.Currencies
{
    public class Euro : BaseCurrency
    {
        public Euro(double value) : base(value, ECurrency.Euro)
        {
        }
        
        public static explicit operator Euro(Dollar dollar) => new Euro(dollar.Value * 2);
    }
}