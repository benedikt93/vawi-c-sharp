﻿using System.Xml.Serialization;

namespace UBXMLSerialisierung.Library
{
    public class Book
    {
        [XmlAttribute("Title")]
        public string Title;

        [XmlAttribute("Author")]
        public string Author;
        
        public BookGenre Genre;
        public int Year;
        public string ISBN;
        public decimal Price;

        public Book(){}
        public Book(string title, string author, BookGenre genre, int year, string isbn, decimal price)
        {
            Title = title;
            Author = author;
            Genre = genre;
            Year = year;
            ISBN = isbn;
            Price = price;
        }
    }
}