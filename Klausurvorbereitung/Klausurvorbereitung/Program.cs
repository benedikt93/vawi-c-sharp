﻿using Klausurvorbereitung.Aufgabe10;
using Klausurvorbereitung.Aufgabe3;
using Klausurvorbereitung.Aufgabe4;
using Klausurvorbereitung.Aufgabe5;
using Klausurvorbereitung.Aufgabe6;
using Klausurvorbereitung.Aufgabe9;

namespace Klausurvorbereitung
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            // Aufgabe3Program.run();
            // Aufgabe4Program.run();
            // Aufgabe5Program.Run();
            // Aufgabe6Program.Run();
            // Aufgabe9Program.Run();
            Aufgabe10Program.Run();
        }
    }
}