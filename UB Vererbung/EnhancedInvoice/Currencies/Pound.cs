﻿namespace EnhancedInvoice.Currencies
{
    public class Pound : BaseCurrency
    {
        public Pound(double value) : base(value, ECurrency.Pound)
        {
        }
    }
}