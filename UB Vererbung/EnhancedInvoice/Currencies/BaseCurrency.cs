﻿using System.Numerics;

namespace EnhancedInvoice.Currencies
{

    public enum ECurrency
    {
        Dollar, Yen, Euro, Pound
    }
    
    public class BaseCurrency
    {
        public double Value;
        public ECurrency Currency;

        public BaseCurrency()
        {
        }

        public BaseCurrency(double value, ECurrency currency)
        {
            Value = value;
            Currency = currency;
        }

        public override string ToString() => Value.ToString();
        /*public static Complex operator +(BaseCurrency c1, BaseCurrency c2)
        {
            return c1.Value + c2.Value;  
        }*/
        
    }
}