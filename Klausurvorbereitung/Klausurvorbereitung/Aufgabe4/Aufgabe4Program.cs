﻿using System;
using System.Collections;

namespace Klausurvorbereitung.Aufgabe4
{
    public class Aufgabe4Program
    {
        public static void run()
        {
            var book1 = new Book(
                BookType.Sachbuch,
                "Eric Freeman",
                "Head First Design Patterns",
                2014
                );
            
            var book2 = new Book(
                BookType.Roman,
                new ArrayList
                {
                    "Kapitel1",
                    "Kapitel2"
                }, 
                "Keine Ahnung",
                "uuuäääähhhh",
                2014
            );
            
            var book3 = new Book(book2);
            book3.Titel = "uuuuäääääähhh - The Revenge";
            book3.Autor = "Der Sohn von Keine Ahnung";
            book3[0] = "Kapitel1.1";
            book3.Add("Kapitel3");
            
            Console.WriteLine(book3[0]);
            Console.WriteLine(book1.ToString());
            Console.WriteLine(book2.ToString());
            Console.WriteLine(book3.ToString());
        }
    }
}