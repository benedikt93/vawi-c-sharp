# Aufgabe 21: XML-Serialisierung
## a) (30 Punkte)
Implementieren Sie eine Assembly, die das Pendant zum abgebildeten XML-Dokument bildet. Das XML-Dokument entspricht
einem Austauschformat mit einem internationalen Parterunternehmen. Im Gegensatz zum englischsprachigen XML-Dokument
erfolgt die Implementierung unter Berücksichtigung der unternehmensinternen Namensgebung, die deutsche Bezeichner für
Klassen, Felder, etc. vorschreibt.

Berücksichtigen Sie bei der Implementierung die Einordnung in einen geeigneten Namespace, eine geeignete Bezeichnung
der Felder, Klassen, etc. Treffen Sie für komplexe Elemente der XML-Struktur entsprechende Implementierungsentscheidungen, um eine möglichst hohe Deckung zwischen XML-Struktur und Klasse zu erreichen.

Beachten Sie bei der Implementierung, dass einige Einträge innerhalb einer Klasse als Methoden bzw. Properties bereitgestellt werden und dynamisch berechnet werden sollen. Treffen Sie angemessene Entscheidungen bzgl. der Datentypen. Es ist
nicht erforderlich, einfache Felder als Properties zu kapseln.

Die Klasse soll zwei Methoden bereitstellen, die die Serialisierung und Deserialisierung kapseln. 
(*ExportToXml(string fileName); ImportFromXml(string fileName)*)

Nutzen Sie XML-Attribute um zu erreichen, dass die Serialisierung dem abgebildeten Ergebnis entspricht. In der Tabelle können Sie hierzu mögliche Attribute nachschlagen.