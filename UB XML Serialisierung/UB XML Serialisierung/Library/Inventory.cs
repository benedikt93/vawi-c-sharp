﻿using System.Linq;
using System.Text;

namespace UBXMLSerialisierung.Library
{
    public class Inventory
    {
        public Book[] Books;

        public decimal TotalValue
        {
         get => Books.Sum(book => book.Price);
         set{}
        }

        public Inventory(){}
        public Inventory(Book[] books)
        {
            Books = books;
        }

        public override string ToString()
        {
            var returnText = new StringBuilder();
            returnText.Append("Titel: ");
            foreach (var book in Books)
            {
                returnText.Append(book.Title + ", ");
            }
            returnText.Append("Gesamtwert: " + TotalValue);
            return returnText.ToString();
        }
    }
}