# Aufgabe 22: XML-Serialisierung
## a) (30 Punkte)
Implementieren Sie eine Assembly „*Warehouse*“, die die für eine Software eingesetzt werden soll, die die oben abgebildete
Struktur zur Verwaltung von Waren einsetzt.
- Die ersten 4 Ebenen sollen als Klassenhierarchie implementiert werden; die fünfte Ebene stellt die Instanzen dar; die
letzte Ebene die Felder der Klasse.
- Die ‚Funktionalität’ der einzelnen Elemente soll in der abstrakten Basisklasse „*Ware*“ implementiert werden.
- Alle abgeleiteten Klassen dienen lediglich der eindeutigen Typisierung und des Aufbaus der Hierarchie.
- Für die ersten drei Ebenen soll verhindert werden, dass Instanzen der Klassen erzeugt werden können.
- Für die Klassen in der Hierarchie müssen Sie keinen Konstruktor implementieren! (Gehen Sie davon aus, dass diese
Aufgabe durch einen anderen Entwickler erledigt wird.)
- Neben den aus der Abbildung ersichtlichen Klassen soll eine Klasse „*WarehouseAdministration*“ erstellt werden,
die Waren über eine Collection verwaltet. Die Klasse soll zwei Methoden bereitstellen, die die Serialisierung und
Deserialisierung kapseln (*ExportToXml(string fileName); ImportFromXml(string fileName)*).
- Nutzen Sie in der Assembly „*Warehouse*“ XML-Attribute um zu erreichen, dass
- der Name einer Ware als Attribut serialisiert wird und
- die Bestandteile der Collection als „*Ware*“ serialisiert werden.

In der Tabelle können Sie hierzu mögliche Attribute nachschlagen.