# Aufgabe 4: Objektorientierung
## a) (30 Punkte)
Implementieren Sie eine Assembly, die ein elektronisches Buch repräsentiert.
Innerhalb des Namespaces *ElectronicLibrary* sollen die Klasse Book und die Enumeration *BookType* implementiert
werden.

Für die Klasse *Book* gelten die folgenden Anforderungen:
- Alle privaten Felder sollen über öffentliche Read-Write-Properties zugreifbar sein.
- In einem *Book* sollen der *Autor*, der *Titel*, das *Erscheinungsjahr* und der *Buchtyp* gespeichert sein.
- Der Inhalt des Buches soll als Collection von einzelnen Kapiteln verwaltet werden. Diese sollen über einen Indexer
abgerufen und über eine Add-Methode hinzugefügt werden können.
- Als Buchtyp soll die Enumeration BookType genutzt werden, die 5 unterschiedliche Buchtypen umfassen soll:
*Roman*, *Gedicht*, *Märchen*, *Sage*, *Sachbuch*
- Implementieren Sie geeignete Konstruktoren
  - Standard-Konstruktor,
  - Copy-Konstruktor,
  - mindestens 2 unterschiedliche Konstruktoren, die eine sinnvolle Erzeugung eines Buches durch direkte
Parameterübergabe ermöglichen
- Ermöglichen Sie eine Ausgabe eines Buches als Zeichenkette über die in C# vorgesehenen Mechanismen.
Dabei sollen ein Buch in einen „XML-String“ serialisiert werden, der die einzelnen Bestandteile der Klasse gemäß dem
folgenden Beispiel enthält, wobei die Punkte durch die Werte des Objektes gefüllt werden sollen:

``
<book> <author>...</author> <title>...</title> <type>...</type> <chapters> <chapter>...</chapter> <chapter>...</chapter>
<chapter>...</chapter> <chapter>...</chapter> <chapter>...</chapter> <chapter>...</chapter> <chapter>...</chapter>
<chapter>...</chapter> </chapters> </book>
``

Erstellen Sie ein Hauptprogramm, das folgende Funktionen der Assembly testet:

Erstellen Sie ein *Book*-Objekt und füllen Sie das Buch mit Inhalt:
- Setzen Sie die Felder über die Properties.
- Füllen Sie das Buch mit einigen „Kapiteln“ Text.
- Geben Sie das Buch auf der Konsole aus.

Greifen Sie über den Indexer auf ein Kapitel zu und geben dieses erneut auf der Konsole aus.