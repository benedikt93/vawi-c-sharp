﻿using Financials;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;

namespace EnhancedInvoice
{
    class EnhancedInvoice : Invoice, IEnumerable<EnhancedInvoiceItem>
    {
        public EnhancedInvoice(DateTime date, UInt32 number, String currency, String recipient)
        {
            Date = date;
            Number = number;
            Currency = currency;
            Recipient = recipient;
        }

        public Double TotalSum
        {
            get
            {
                Double sum = 0;
                foreach (EnhancedInvoiceItem item in Items)
                {
                    sum += item.SumPrice;
                }
                return sum;
            }
        }

        public EnhancedInvoiceItem this[int i] => Items[i];

        public override string ToString() => "Rechnungssumme: " + TotalSum;

        public new EnhancedInvoiceItem[] Items => base.Items.ToArray( typeof(EnhancedInvoiceItem)) as EnhancedInvoiceItem[];
        
        // Aufgabe 7
        public IEnumerator<EnhancedInvoiceItem> GetEnumerator()
        {
            foreach (var item in Items)
            {
                yield return item;       
            }
        }
        
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
        
        
    }

    class EnhancedInvoiceItem : InvoiceItem
    {
        public EnhancedInvoiceItem(UInt32 stocknumber, UInt32 quantity, String description, double unitPrice)
        {
            Stocknumber = stocknumber;
            Quantity = quantity;
            Description = description;
            UnitPrice = unitPrice;
        }

        public override string ToString()
        {
            return Description + "(" + Stocknumber + "): Anzahl " + Stocknumber + ", Stückpreis " + UnitPrice;
        }
    }
}

/*
  (x) 1 Konstruktor EnhancedInvoice
  (x) 2 Konstruktor EnhancedInvoiceIdem
  (x) 3 ToString Methode für EnhancedinvoiceItem
  (x) 4 Enhanced Invoice
  (x)     - rOnly Property TotalSum
  (x)     - rOnly Indexer
  (x)     - ToString
  (x)     - Override Items
 */