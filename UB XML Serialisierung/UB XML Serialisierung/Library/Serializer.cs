﻿using System;
using System.IO;
using System.Xml.Serialization;

namespace UBXMLSerialisierung.Library
{
    public class Serializer
    {
        private XmlSerializer serializer = new XmlSerializer(typeof(Inventory));
        
        public void ExportToXml(string fileName, Inventory inventory)
        {
            using (var writer = new StreamWriter(fileName))
            {
                serializer.Serialize(writer, inventory);
                Console.WriteLine("File exported");
            }
        }

        public Inventory ImportFromXml(string fileName)
        {
            using (var reader = new StreamReader(fileName))
            {
                return (Inventory) serializer.Deserialize(reader);
            }
        }
    }
}