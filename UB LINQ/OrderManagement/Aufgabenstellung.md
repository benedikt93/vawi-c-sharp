# UB 11 LINQ
## Qualifikationsziele
Können LINQ-Queries ausführen, um zielgerichtet auf Datenquellen
zuzugreifen 

## Aufgabe
 - Laden Sie die bereitgestellte Projektmappe herunter
und machen Sie sich mit der Struktur der Anwendung vertraut
 - Implementieren Sie die in der Program.cs
und auf der nachfolgenden Folie aufgelisteten Queries
und die entsprechende Ausgabe in der Konsole
 - Erstellen Sie eigene Queries
 
## Aufgabe
 1. Liefert alle Produktnamen in der Datenbank in aufsteigender Reihenfolge (als Beispiel bereits implementiert)
 2. Liefert alle Nachnamen der Kunden, die eine Bestellung getätigt haben
 3. Liefert alle bestellten Produkten und die jeweilige Anzahl
 4. Liefert alle Nachnamen der Kunden mit der jeweiligen Gesamtsumme der getätigten Bestellungen
 5. Liefert den Nachnamen des Kunden mit dem höchsten Gesamtgewicht aller Bestellungen
 6. Liefert den Namen und das Gewicht des teuersten Produkts in der Datenbank