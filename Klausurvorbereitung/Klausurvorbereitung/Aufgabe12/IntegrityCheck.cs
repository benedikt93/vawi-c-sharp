﻿using System.Text.RegularExpressions;

namespace Klausurvorbereitung.Aufgabe12
{
    public class IntegrityCheck
    {
        public static bool checkTelephoneNumber(string number)
        {
            var regex = @"^\d{2,5}\-\d{3,7}$";
            return Regex.IsMatch(number, regex);
        }
        public static bool checkEmail(string email)
        {
            var regex = @"^\w+(\.\w+)?@\w[\w\.\-]*\.\w{2,5}$";
            return Regex.IsMatch(email, regex);
        }
    }
}