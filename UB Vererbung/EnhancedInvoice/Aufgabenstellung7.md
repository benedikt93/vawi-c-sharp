# UB 07 Vererbung
## Qualifikationsziele
Die Studierenden
- Können das Interface IEnumerable implementieren und mit einer
foreach-Schleife durchlaufen
- Können implizite und explizite Casts durchführen und Operatoren
überladen

## Iteratoren
### Ziele der Aufgabe
- Ziel dieser Aufgabe ist die eigenständige Erarbeitung der
notwendigen Kenntnisse und anschließende Implementierung des
Interfaces IEnumerable für eine Klasse, damit die Elemente in
der Klasse über die foreach-Schleife durchlaufen werden können.
- Die Aufgabe baut auf der Klasse EnhancedInvoice auf, damit
keine neue Klasse implementiert werden muss.
- Informationen und ein Beispiel zu IEnumerable sind in der MSDN
unter „Using Iterators “ zu finden.

### Aufgabenbeschreibung
- Die Klasse EnhancedInvoice soll ergänzt werden, so dass über eine
foreach-Schleife die einzelnen EnhancedInvoiceItems abgerufen
werden können.
- Bisher ist dies im Hauptprogramm über die folgende Syntax möglich:
foreach(EnhancedInvoiceItem eit in myInvoice.Items)
- Mit der Unterstützung des IEnumerable-Interfaces soll die folgende
Syntax ermöglicht werden:
foreach(EnhancedInvoiceItem eit in myInvoice)
- Testen Sie die neu implementierte Funktionalität im Hauptprogramm.

## Operatorüberladung
### Ziele der Aufgabe
- Ziel der Aufgabe ist es Berechnungen mit einzelnen
Währungsobjekten durchführen zu können.
- Die Umsetzung erfolgt anhand von impliziten und expliziten Type
Casts und Operatorüberladungen.
- Informationen und Beispiele zu Casts finden Sie bei Nagel (2016,
Kapitel 8 - S. 233ff.).

### Aufgabenbeschreibung
**Währungsumrechnung - mit expliziten Casts**
1. Implementieren Sie eine Klasse BaseCurrency mit einem Feld
Value(double) und einem Feld Currency(Enum). Die Enumeration
beinhaltet vier Währungen.
2. Implementieren Sie die Klassen Dollar, Yen, Euro und Pound, welche
von BaseCurrency abgeleitet sind.
3. Implementieren Sie explizite Cast-Operatoren, so dass eine
Währungsumwandlung zwischen den Klassen mittels expliziter Casts
möglich ist. Bei einem Cast zwischen den Währungen, soll der
entsprechende Umrechnungsfaktor verwendet werden.
4. Erzeugen Sie entsprechende Objekte, mit denen Sie Ihre Anwendung
testen können.

**Währungsumrechnung - mit impliziten Casts**
5. Passen Sie die Implementierung an und verwenden Sie implizite
Type Casts, so dass eine Währungsumwandlung zwischen den
Klassen mittels impliziter Casts möglich ist.
6. Implementieren Sie auch Operatorüberladungen, um einzelne
Objekte addieren und subtrahieren zu können, z.B. in der Form:
``MyDollar = MyYen + MyEuro;``

